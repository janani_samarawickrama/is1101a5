#include <stdio.h>


int attend(int price)
{
    return 120-(price-15)/5 * 20;
}
int revenue(int price)
{
    return price * attend(price);
}
int cost(int price)
{
    return 500+(3 * attend(price));
}
int profit(int price)
{
    return revenue(price)-cost(price);
}
int main()
{
    int price;
    printf("Enter the ticket price: ");
    scanf("%d",&price);
    printf("Profit = %d",profit(price));
    return 0;
}
